//println Calendar.MONTH
def daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31]
def startDate = new Date().parse("dd/MM/yyyy", "23/03/2017")
def today = new Date()
if (startDate[Calendar.YEAR] % 4 == 0) daysInMoth[1] = 29
def monthOfToday = (today[Calendar.MONTH] > startDate[Calendar.MONTH])? today[Calendar.MONTH] : today[Calendar.MONTH]+12
def totalDays = 0
(startDate[Calendar.MONTH]..(monthOfToday - 1)).each {
    totalDays += daysInMonth[it]
}
totalDays += today[Calendar.DAY_OF_MONTH] - startDate[Calendar.DAY_OF_MONTH]
def weeks = (totalDays / 7) as int
def days = (totalDays % 7) as int
println "Until ${today.format('dd-MM-yy')} it's ${weeks} weeks and ${days} days, total ${totalDays} days"