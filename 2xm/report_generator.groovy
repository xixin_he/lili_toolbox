def team = "fin"
File dataSource = new File("/Users/xihe/Desktop/Workbook3.csv")
File nameDictionary = new File("/Users/xihe/Desktop/${team}_name_list.csv")
def nameCheck = [:]
nameDictionary.eachLine { line ->
    def namePair =  line.tokenize(",")
    nameCheck[namePair[0]] = namePair[1]
}

def itemCode = ""
def item = [:]
dataSource.eachLine {line ->
    def entry = line.tokenize(",")
    def nameString = entry[0].replaceAll(/ /, "")
    println nameString
    def name = []
    if (entry[2] != null ) {
        itemCode = entry[2]
        item[itemCode] = [:]
    }
    def amount = entry[1]
    switch (nameString) {
        case ~/WA-/: println 'WA'; break;
        case ~/(\w+)(\d+)-(\w+)(\d+)/: println 'share with percentage'; break;
        case ~/(\w+)-(\w+)/: println 'share'; break;
        default: println 'alone'
    }
}