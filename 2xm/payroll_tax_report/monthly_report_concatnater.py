# coding=utf-8

'''
This tool is to concatenate the payroll tax reports in this directory. 
All report name must be in 'Report-mm-yyyy.CSV' format
The concatenated file will be on Desktop named 'payroll_tax_file_yyyymm-yyyymm.xlsx'
'''

print(__doc__)
proceed = input('Continue? (y/n)')
if proceed == 'n':
    print('exit!')
    exit()

from pathlib import Path
import pandas as pd

month_range = input('Enter start month and end month(eg. 201801-201809): ')
if '-' not in month_range:
    raise Exception('please specify start month and end month')
start_month = int(month_range.split('-')[0])
end_month = int(month_range.split('-')[1])
if end_month < start_month :
    raise Exception('End month cannot be earlier than start month')
    
monthly_report_name = dict([(201800 + i, str(f'-{i:0>2}-2018')) for i in range(1, 13)])
monthly_report_name.update(dict([(201700 + i, str(f'-{i:0>2}-2017')) for i in range(2, 13)]))
monthly_report_name.update(dict([(201900 + i, str(f'-{i:0>2}-2017')) for i in range(1, 3)]))

selected_monthly_report_name = {k:v for k, v in monthly_report_name.items() if start_month <= k <= end_month}
# print(selected_report_name)

base_path = Path("./")

final_df = pd.DataFrame([], columns=['Pay Through Entity', 'Branch', 'Division', 'Payable State', 'Tax Rate',
       'Customer ID', 'Customer', 'Job Name', 'Workplace State', 'Employee ID',
       'MID', 'Employee Surname', 'Employee First Name',
       'Employee Pay Type Name', ' Permission Group', 'Eligible',
       'Total Gross wages', 'Total Super', 'Total Wages',
       'Gross Taxable Wages', 'Taxable Super', 'Total Taxable Wages'])

total_line = 0
for k, v in selected_monthly_report_name.items():
    report_file = base_path / f'Report{v}.CSV'
    
    if report_file.exists():
        print(report_file)
        df = pd.read_csv(report_file)
        total_line += len(df)
        final_df = final_df.append(df, ignore_index=True)
        
if total_line == len(final_df):
    print(f'line number matches, total is {total_line} lines')
    target_report = Path.home() / "Desktop" / f"payroll_tax_report_{month_range}.xlsx"
    final_df.to_excel(target_report, index=False)
else:
    print(f'line number does not match, total_line {total_line}, final_df {len(final_df)}')
